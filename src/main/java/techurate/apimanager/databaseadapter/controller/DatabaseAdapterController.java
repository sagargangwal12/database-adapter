package techurate.apimanager.databaseadapter.controller;


import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import techurate.apimanager.metadata.DatabaseDetails;
import techurate.apimanager.metadata.Pagination;
import techurate.apimanager.repository.DatabaseDetailRepository;
import techurate.apimanager.repository.ServiceDefinitionRepository;
import techurate.apimanager.service.DatabaseAdapterService;

import java.util.Optional;

@RestController
@RequestMapping("/apimanager/databaseadpater")
public class DatabaseAdapterController {

    @Autowired
    private DatabaseAdapterService databaseAdapterService;

    @Autowired
    private ServiceDefinitionRepository serviceDefinitionRepository;

    @Autowired
    private DatabaseDetailRepository databaseDetailRepository;

    @PostMapping("/databaseadapter/execute")
    public ResponseEntity<Object> executeDatabaseAdapterServiceByServiceName(@RequestBody Optional<String> payloadStr,
                                                                             @RequestParam(required = false) String pagination,
                                                                             @RequestParam(required = false) Integer fromPage,
                                                                             @RequestParam(required = false) Integer toPage) throws Exception {


        Object returnObj = null;
        Pagination pagination1=null;
        String payloadTransaction = "";
        if (payloadStr.isPresent()) {
            payloadTransaction = payloadStr.get();
        }
        if(pagination != null && pagination.equalsIgnoreCase("true")){
            pagination1=Pagination.builder().fromPage(fromPage).toPage(toPage).isPaginationRequired("true").build();
        }

        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(payloadTransaction);

        JSONObject payloadHeaders = (JSONObject) json.get("header");
        JSONObject ids = databaseAdapterService.getServiceAndOperationIdsByNames(payloadHeaders.get("serviceName").toString(), payloadHeaders.get("operationName").toString());

        System.out.println("Inside execute service " + ids);
        if (ids == null) {
            return new ResponseEntity<Object>("{\"status\": \"failure\", \"message\": \"Invalid Service Name or Operation Name\"}", new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        String serviceID = ids.get("serviceId").toString();
        System.out.println("serviceID====>"+serviceID);
        String operationID = ids.get("operationId").toString();

        returnObj = databaseAdapterService.processRequest(pagination1,payloadTransaction, serviceID, operationID, payloadHeaders);
        System.out.println("service" + returnObj.toString());
        return new ResponseEntity<Object>(returnObj, new HttpHeaders(), HttpStatus.OK);
    }


    @PostMapping(value = "/databaseDetails", produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String saveDetails(@RequestBody DatabaseDetails databaseDetails) {
        databaseDetailRepository.save(databaseDetails);
        return databaseDetails.toString();
    }

}
