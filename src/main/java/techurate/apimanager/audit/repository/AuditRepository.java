package techurate.apimanager.audit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import techurate.apimanager.audit.AuditTrail;

public interface AuditRepository extends JpaRepository<AuditTrail, String> {
}
