package techurate.apimanager.audit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "audittrail")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuditTrail {

    @Id
    @Column(name = "id")
    private String auditTrailId;

    @Lob
    private String sourceRequest;

    @Lob
    private String destinationRequest;

    @Lob
    private String sourceResponse;

    @Lob
    private String destinationResponse;

    @CreatedBy
    private String userName;

    @CreatedDate
    private Date auditedDateTime;

    @LastModifiedBy
    private String modifedBy;

    @LastModifiedDate
    private Date auditModifieddDateTime;

    private Date sourceRequestDate;

    private Date sourceResponseDate;

    private Date destinationRequestDate;

    private Date destinationResponseDate;

    private String status;

    private String serviceID;

    private String operationID;

    private String serviceType;

}
