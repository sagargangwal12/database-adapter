package techurate.apimanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabaseAdapterApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatabaseAdapterApplication.class, args);
    }
}
