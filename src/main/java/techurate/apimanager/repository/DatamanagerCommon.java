package techurate.apimanager.repository;

import org.springframework.stereotype.Component;
import techurate.apimanager.metadata.DatabaseDetails;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DatamanagerCommon {

    private static Statement statement;
    private static ResultSet resultSet;


    public List<Map<String, String>> getConnectionAndExecute(String databaseUrl, String userName, String password, String driverName, String queryString) {
//        String databaseURL = "jdbc:oracle:thin:@//199.188.207.140:1521/FCUBSDEV";
//        String user = "ZICBTEKUAT";
//        String password = "ZICBTEKUAT";
//        String drivers = "oracle.jdbc.OracleDriver";
        Connection conn = null;
        List<Map<String, String>> mapList = null;
        try {
            Class.forName(driverName);
            conn = DriverManager.getConnection(databaseUrl, userName, password);
            if (conn != null) {
                System.out.println("Connected to the database");
                statement = conn.createStatement();
                resultSet = statement.executeQuery(queryString);
                mapList = getList(resultSet);
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Could not find database driver class");
            ex.printStackTrace();
        } catch (SQLException ex) {
            System.out.println("An error occurred. Maybe user/password is invalid");
            ex.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return mapList;
    }

    public List<Map<String, String>> getListByQuery(String queryString, DatabaseDetails databaseDetails) throws SQLException {
        return getConnectionAndExecute(databaseDetails.getDatabaseUrl(),
                databaseDetails.getDatabaseUserName(),
                databaseDetails.getPassword(),
                databaseDetails.getDatabaseDriverName(),
                queryString);

    }

    public List<Map<String, String>> getList(ResultSet resultSet) throws SQLException {
        List<Map<String, String>> mapList = new ArrayList<>();
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        int columnNumber = resultSetMetaData.getColumnCount();
        while (resultSet.next()) {
            Map<String, String> map = new HashMap<>();
            int count = 1;
            while (count <= columnNumber) {
                String columnName = resultSetMetaData.getColumnName(count);
                map.put(columnName, resultSet.getString(columnName));
                count++;
            }
            mapList.add(map);
        }
        System.out.println(mapList);
        return mapList;
    }
}
