package techurate.apimanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import techurate.apimanager.metadata.DatabaseDetails;

public interface DatabaseDetailRepository extends JpaRepository<DatabaseDetails, String> {
    DatabaseDetails findByServiceIDAndOperationID(String serviceID, String operationID);
}
