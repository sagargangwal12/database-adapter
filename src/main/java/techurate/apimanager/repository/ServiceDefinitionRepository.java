package techurate.apimanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import techurate.apimanager.metadata.ServiceDefinition;

@Repository
public interface ServiceDefinitionRepository extends JpaRepository<ServiceDefinition, String> {

    @Query(value = "select sdf from ServiceDefinition sdf where sdf.name = :serviceName")
    ServiceDefinition findServiceDefinitionByName(@Param("serviceName") String serviceName);

}
