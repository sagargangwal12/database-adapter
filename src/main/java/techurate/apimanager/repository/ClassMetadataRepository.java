package techurate.apimanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import techurate.apimanager.metadata.ClassMetadata;

import java.util.Set;

@Repository
public interface ClassMetadataRepository extends JpaRepository<ClassMetadata, String> {
    Set<ClassMetadata> findByServiceID(String serviceID);
}
