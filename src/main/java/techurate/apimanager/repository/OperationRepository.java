package techurate.apimanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import techurate.apimanager.metadata.Operation;

@Repository
public interface OperationRepository extends JpaRepository<Operation, String> {
}
