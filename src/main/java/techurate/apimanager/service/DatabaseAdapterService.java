package techurate.apimanager.service;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import techurate.apimanager.repository.DatamanagerCommon;
import techurate.apimanager.audit.AuditTrail;
import techurate.apimanager.audit.repository.AuditRepository;
import techurate.apimanager.exception.RecordNotFoundException;
import techurate.apimanager.metadata.*;
import techurate.apimanager.repository.DatabaseDetailRepository;
import techurate.apimanager.repository.OperationRepository;
import techurate.apimanager.repository.ServiceDefinitionRepository;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@Service
public class DatabaseAdapterService {

    @Autowired
    private ServiceDefinitionRepository repository;

    @Autowired
    private DatabaseDetailRepository databaseDetailRepository;

    @Autowired
    private OperationRepository operationRepository;

    @Autowired
    private DatamanagerCommon datamanagerCommon;

    @Autowired
    private AuditRepository auditRepository;

    @Autowired
    Environment env;
    Logger logger = LoggerFactory.getLogger(DatabaseAdapterService.class);

    @SuppressWarnings("unchecked")
    public JSONObject getServiceAndOperationIdsByNames(String serviceName, String operationName) {
        JSONObject json = null;
        try {
            ServiceDefinition definition = repository.findServiceDefinitionByName(serviceName);
            if (definition != null) {
                json = new JSONObject();
                json.put("serviceId", definition.getServiceID());
                Optional<Operation> opr = definition.getOperations().stream().filter(op -> op.getAppOperationName() != null && operationName.equalsIgnoreCase(op.getAppOperationName())).findFirst();
                json.put("operationId", opr.get().getOperationId());
            }
        } catch (Exception e) {
            logger.error("error at  findServiceDefinitionByNameAndVersion ", e);
        }
        return json;
    }

    public ServiceDefinition getService(String serviceID) {
        return repository.getOne(serviceID);
    }

    public Object processRequest(Pagination pagination, String payload, String serviceId, String operationId, JSONObject payloadHeaders) throws Exception {
        Optional<ServiceDefinition> sd1 = repository.findById(serviceId);
        ServiceDefinition service = null;
        if (sd1.isPresent()) {
            service = sd1.get();
            System.out.println("Service Name" + service.getName());
            if (!service.getStatus().equals(StatusType.ACTIVE)) {
                logger.error("Service : " + service.getName() + " is not in the active state.");
                throw new RecordNotFoundException("Service does not exist!");
            }
        } else {
            logger.error("Service : " + serviceId + " not found!!!");
            throw new RecordNotFoundException("Service is invalid!!");
        }
        logger.info("Started the execution for the " + service.getName());

        AuditTrail audit = new AuditTrail();
        audit.setSourceRequest(payload);
        audit.setServiceID(service.getServiceID());
        audit.setOperationID(operationId);
        audit.setAuditTrailId(UUID.randomUUID().toString());
        audit.setServiceType(service.getServiceType().name());
        audit.setSourceRequest(payload);
        auditRepository.save(audit);

        return processDatabaseAdapter(pagination, payload, service, operationId, audit, payloadHeaders);
    }

    public Object processDatabaseAdapter(Pagination pagination, String payload, ServiceDefinition service, String operationId, AuditTrail audit, JSONObject payloadHeaders) {
        Object sourceResponse = null;
        Operation operation = null;
        try {
            Optional<Operation> operationDBObj = operationRepository.findById(operationId);
            if (operationDBObj.isPresent()) {
                operation = operationDBObj.get();
                DatabaseDetails databaseDetails = databaseDetailRepository.findByServiceIDAndOperationID(service.getServiceID(), operation.getOperationId());
                System.out.println("DatabaseDetails" + databaseDetails.toString());
                sourceResponse = invokeDatabaseAdapter(pagination, payload, service, operationId, audit, payloadHeaders, databaseDetails);
            } else {
                logger.error("Operation : " + operationId + " not found!!!");
                throw new RecordNotFoundException("Operation is invalid!!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sourceResponse;
    }

    public List<Map<String, String>> invokeDatabaseAdapter(Pagination pagination,
                                                           String payload,
                                                           ServiceDefinition service,
                                                           String operationId,
                                                           AuditTrail audit,
                                                           JSONObject payloadHeaders,
                                                           DatabaseDetails databaseDetails) {
        List<Map<String, String>> response = null;
        try {
            System.out.println("Input jason" + payload);

            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(payload);
            JSONObject payload1 = (JSONObject) json.get("bodydetails");
            StringBuilder queryToRun = new StringBuilder(payload1.get("dynamicQuery").toString());
            if (pagination != null && pagination.getIsPaginationRequired().equalsIgnoreCase("true")) {
                if (databaseDetails.getDatabaseType().equalsIgnoreCase("oracle")) {
                    queryToRun.append(" OFFSET ").append(pagination.getFromPage()).append(" ROWS FETCH NEXT ").append(pagination.getToPage()).append(" ROWS ONLY");
                } else if (databaseDetails.getDatabaseType().equalsIgnoreCase("mysql")) {
                    queryToRun.append(" limit ").append(pagination.getFromPage()).append(" ,").append(pagination.getToPage());
                }
            }
            System.out.println("queryToRun ==>" + queryToRun.toString());
            response = datamanagerCommon.getListByQuery(queryToRun.toString(), databaseDetails);

            System.out.println("------------------" + response);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;

    }

}
