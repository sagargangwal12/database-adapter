package techurate.apimanager.config;

import org.springframework.context.EnvironmentAware;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.*;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.stream.StreamSupport;

public class PropertySourceConfig extends PropertySourcesPlaceholderConfigurer implements EnvironmentAware {
    private static final String APP_CONFIG_NAME = "runtime.properties";
    private static final String APP_CONFIG_PATH = Optional.ofNullable(System.getProperty("CONFIG_DIR")).orElseThrow(() -> new RuntimeException("CONFIG_DIR must be config."));

    private static Environment environment;

    @Override
    public void setEnvironment(Environment environment) {
        super.setEnvironment(environment);
        this.environment = environment;
        this.prepareProperties();
        configureWatcher();
        File applicationFolder = Paths.get(environment.getProperty("applicationJarFileLocation")).toFile();
        if (!applicationFolder.exists()) {
            applicationFolder.mkdir();
        }
    }

    public static String getProperty(String key) {
        return environment.getProperty(key);
    }

    private void configureWatcher() {
        try {
            final WatchService watchService = FileSystems.getDefault().newWatchService();
            Executors.newSingleThreadExecutor().execute(() -> {
                try {
                    Path path = Paths.get(APP_CONFIG_PATH);
                    path.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
                    WatchKey watchKey;
                    while ((watchKey = watchService.take()) != null) {
                        watchKey.pollEvents().stream().forEach(event -> {
                            if (event.context().toString().equals(APP_CONFIG_NAME)) {
                                this.prepareProperties();
                                Paths.get(environment.getProperty("applicationJarFileLocation")).toFile().mkdir();
                            }
                        });
                        watchKey.reset();
                    }
                } catch (Exception e) {
                }
            });

            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    try {
                        watchService.close();
                    } catch (Exception e) {
                    }
                }
            });
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void prepareProperties() {
        if (environment != null) {
            final ConfigurableEnvironment configEnv = ((ConfigurableEnvironment) environment);
            final MutablePropertySources propSources = configEnv.getPropertySources();
            Optional<PropertySource<?>> appConfig = StreamSupport.stream(propSources.spliterator(), false).filter(ps -> ps.getName().matches("^.*runtime.*")).findFirst();
            if (!appConfig.isPresent()) {
                propSources.addLast(new PropertiesPropertySource("runtime", this.getProperties()));
            }
            if (appConfig.isPresent()) {
                propSources.replace("runtime", new PropertiesPropertySource("runtime", this.getProperties()));
            }
        }
    }

    private Properties getProperties() {
        Properties properties = new Properties();
        System.out.println("Location : " + APP_CONFIG_PATH + " :::::: " + APP_CONFIG_NAME);
        try {
            FileSystemResource fsr = new FileSystemResource(new File(APP_CONFIG_PATH, APP_CONFIG_NAME));
            PropertiesLoaderUtils.fillProperties(properties, fsr);
        } catch (IOException ioE) {
            ioE.printStackTrace();
            throw new RuntimeException("Fail to load properties from resources");
        }
        return properties;
    }
}
