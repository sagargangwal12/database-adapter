package techurate.apimanager.metadata;

public enum StatusType {
    ACTIVE(0), INACTIVE(1), DELETED(2);

    private int status;

    private StatusType(int status) {
        this.setStatus(status);
    }

    public String value() {
        return name();
    }

    public static StatusType fromValue(String v) {
        return valueOf(v);
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
