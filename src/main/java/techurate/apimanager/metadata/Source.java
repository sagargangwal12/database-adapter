package techurate.apimanager.metadata;

public enum Source {
    XSD("XSD"), JSON("JSON"), ISO("ISO"), SQL("SQL");

    private String type;

    private Source(String type) {
        this.setType(type);
    }

    public String value() {
        return name();
    }

    public static Source fromValue(String v) {
        return valueOf(v);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
