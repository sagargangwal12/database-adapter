package techurate.apimanager.metadata;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(indexes = @Index(
        name = "idx_operation_servicedefinitionid",
        columnList = "servicedefinition_serviceid",
        unique = false
))
public class Operation {

    @Id
    @Column(name = "id")
    @JsonProperty("operationId")
    private String operationId;

    @JsonProperty("name")
    private String name;

    @JsonProperty("soapAction")
    private String soapAction;

    @JsonProperty("appOperationName")
    private String appOperationName;

    @Transient
    @JsonProperty("requestJSONLocation")
    private String requestJSONLocation;

    @Transient
    @JsonProperty("responseJSONLocation")
    private String responseJSONLocation;

    @Transient
    @JsonProperty("requestClass")
    private String requestClass;

    @Transient
    @JsonProperty("responseClass")
    private String responseClass;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "operationid")
    @JsonProperty("classMetadata")
    private Set<ClassMetadata> classMetadata = null;

    @CreatedDate
    private Date createdDate;

    @CreatedBy
    private String createdBy;

    @LastModifiedDate
    private Date updatedDate;

    @LastModifiedBy
    private String modifiedBy;


    @JsonProperty("operationId")
    public String getOperationId() {
        return operationId;
    }

    @JsonProperty("operationId")
    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("soapAction")
    public String getSoapAction() {
        return soapAction;
    }

    @JsonProperty("soapAction")
    public void setSoapAction(String soapAction) {
        this.soapAction = soapAction;
    }

    @JsonProperty("classMetadata")
    public Set<ClassMetadata> getClassMetadata() {
        return classMetadata;
    }

    @JsonProperty("classMetadata")
    public void setClassMetadata(Set<ClassMetadata> classMetadata) {
        this.classMetadata = classMetadata;
    }

    @JsonProperty("appOperationName")
    public String getAppOperationName() {
        return appOperationName;
    }

    @JsonProperty("appOperationName")
    public void setAppOperationName(String appOperationName) {
        this.appOperationName = appOperationName;
    }

    @JsonProperty("responseJSONLocation")
    public String getResponseJSONLocation() {
        return responseJSONLocation;
    }

    @JsonProperty("responseJSONLocation")
    public void setResponseJSONLocation(String responseJSONLocation) {
        this.responseJSONLocation = responseJSONLocation;
    }

    @JsonProperty("requestJSONLocation")
    public String getRequestJSONLocation() {
        return requestJSONLocation;
    }

    @JsonProperty("requestJSONLocation")
    public void setRequestJSONLocation(String requestJSONLocation) {
        this.requestJSONLocation = requestJSONLocation;
    }

    @JsonProperty("requestClass")
    public String getRequestClass() {
        return requestClass;
    }

    @JsonProperty("requestClass")
    public void setRequestClass(String requestClass) {
        this.requestClass = requestClass;
    }

    @JsonProperty("responseClass")
    public String getResponseClass() {
        return responseClass;
    }

    @JsonProperty("responseClass")
    public void setResponseClass(String responseClass) {
        this.responseClass = responseClass;
    }

    public String toString() {
        return this.name;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}