package techurate.apimanager.metadata;

public enum SystemType {
    FLEXCUBE("FCUBS"), PAYTM("PAYTM");

    private String type;

    private SystemType(String type) {
        this.setType(type);
    }

    public String value() {
        return name();
    }

    public static SystemType fromValue(String v) {
        return valueOf(v);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
