package techurate.apimanager.metadata;

public enum ServiceType {
    JSON2SOAP("Json2Soap"), JSON2ISO("Json2ISO");

    private String type;

    private ServiceType(String type) {
        this.setType(type);
    }

    public String value() {
        return name();
    }

    public static ServiceType fromValue(String v) {
        return valueOf(v);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
