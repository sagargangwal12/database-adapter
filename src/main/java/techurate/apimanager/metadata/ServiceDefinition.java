package techurate.apimanager.metadata;

import com.fasterxml.jackson.annotation.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "servicedefinition", uniqueConstraints =
@UniqueConstraint(columnNames = {"name", "version"}))
public class ServiceDefinition {

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("packageName")
    private String packageName;

    @JsonProperty("schemaLocation")
    private String schemaLocation;

    @JsonProperty("wsdlLocation")
    private String wsdlLocation;

    @JsonProperty("version")
    private Double version;

    @JsonProperty("endpointURL")
    private String endpointURL;

    @JsonProperty("requestJSONclasses")
    @Column(length = 30000)
    private String requestJSONclasses = null;

    @JsonProperty("responseJSONclasses")
    @Column(length = 30000)
    private String responseJSONclasses = null;

    @JsonProperty("classes")
    @Column(length = 200000)
    private String classes = null;

    @Column(length = 80000)
    @JsonProperty("operations")
    @OneToMany(cascade = CascadeType.ALL)
    @OrderBy(value = "name ASC")
    @JoinColumn(name = "servicedefinition_serviceid")
    private Set<Operation> operations;

    @JsonProperty("isoMetadata")
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "serviceID")
    private Set<IsoMapping> isoMetadata;

    @JsonProperty("applicationServiceName")
    private String applicationServiceName;

    @JsonProperty("requestHeaderClass")
    private String requestHeaderClass;

    @JsonProperty("responseHeaderClass")
    private String responseHeaderClass;

    @JsonIgnore
    @Transient
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("soapVersion")
    private String soapVersion;

    @JsonProperty("status")
    @Enumerated(EnumType.ORDINAL)
    @Column(columnDefinition = "integer default 0")
    private StatusType status = StatusType.ACTIVE;

    @Id
    @Column(name = "id")
    @JsonProperty("serviceID")
    private String serviceID;

    @CreatedDate
    private Date createdDate;

    @CreatedBy
    private String createdBy;

    @LastModifiedDate
    private Date updatedDate;

    @LastModifiedBy
    private String modifiedBy;

    @Column(name = "type")
    @JsonProperty("serviceType")
    @Enumerated(EnumType.STRING)
    private ServiceType serviceType;

    @JsonProperty("backendSystem")
    @Enumerated(EnumType.STRING)
    private SystemType backendSystem;

    @Transient
    @JsonProperty("constructISOMessage")
    private boolean constructISOMessage;

    @Transient
    @JsonProperty("parseISOMessage")
    private boolean parseISOMessage;

    @JsonProperty("enrichmentEnabled")
    private boolean enrichmentEnabled;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("packageName")
    public String getPackageName() {
        return packageName;
    }

    @JsonProperty("packageName")
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @JsonProperty("schemaLocation")
    public String getSchemaLocation() {
        return schemaLocation;
    }

    @JsonProperty("schemaLocation")
    public void setSchemaLocation(String schemaLocation) {
        this.schemaLocation = schemaLocation;
    }

    @JsonProperty("version")
    public Double getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(Double version) {
        this.version = version;
    }

    @JsonProperty("endpointURL")
    public String getEndpointURL() {
        return endpointURL;
    }

    @JsonProperty("endpointURL")
    public void setEndpointURL(String endpointURL) {
        this.endpointURL = endpointURL;
    }

    @JsonProperty("classes")
    public String getClasses() {
        return classes;
    }

    @JsonProperty("classes")
    public void setClasses(String classes) {
        this.classes = classes;
    }

    @JsonProperty("operations")
    public Set<Operation> getOperations() {
        return operations;
    }

    @JsonProperty("operations")
    public void setOperations(Set<Operation> operations) {
        this.operations = operations;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonProperty("serviceID")
    public String getServiceID() {
        return serviceID;
    }

    @JsonProperty("serviceID")
    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    @JsonProperty("serviceType")
    public ServiceType getServiceType() {
        return serviceType;
    }

    @JsonProperty("serviceType")
    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    @JsonProperty("isoMetadata")
    public Set<IsoMapping> getISOMetadata() {
        return isoMetadata;
    }

    @JsonProperty("isoMetadata")
    public void setISOMetadata(Set<IsoMapping> isoMetadata) {
        this.isoMetadata = isoMetadata;
    }

    @JsonProperty("soapVersion")
    public String getSoapVersion() {
        return soapVersion;
    }

    @JsonProperty("soapVersion")
    public void setSoapVersion(String soapVersion) {
        this.soapVersion = soapVersion;
    }

    @JsonProperty("requestJSONclasses")
    public String getRequestJSONclasses() {
        return requestJSONclasses;
    }

    @JsonProperty("requestJSONclasses")
    public void setRequestJSONclasses(String requestJSONclasses) {
        this.requestJSONclasses = requestJSONclasses;
    }

    @JsonProperty("responseJSONclasses")
    public String getResponseJSONclasses() {
        return responseJSONclasses;
    }

    @JsonProperty("responseJSONclasses")
    public void setResponseJSONclasses(String responseJSONclasses) {
        this.responseJSONclasses = responseJSONclasses;
    }

    @JsonProperty("applicationServiceName")
    public String getApplicationServiceName() {
        return applicationServiceName;
    }

    @JsonProperty("applicationServiceName")
    public void setApplicationServiceName(String applicationServiceName) {
        this.applicationServiceName = applicationServiceName;
    }

    @JsonProperty("requestHeaderClass")
    public String getRequestHeaderClass() {
        return requestHeaderClass;
    }

    @JsonProperty("requestHeaderClass")
    public void setRequestHeaderClass(String requestHeaderClass) {
        this.requestHeaderClass = requestHeaderClass;
    }

    @JsonProperty("responseHeaderClass")
    public String getResponseHeaderClass() {
        return responseHeaderClass;
    }

    @JsonProperty("responseHeaderClass")
    public void setResponseHeaderClass(String responseHeaderClass) {
        this.responseHeaderClass = responseHeaderClass;
    }

    @JsonProperty("backendSystem")
    public SystemType getBackendSystem() {
        return backendSystem;
    }

    @JsonProperty("backendSystem")
    public void setBackendSystem(SystemType backendSystem) {
        this.backendSystem = backendSystem;
    }

    @JsonProperty("wsdlLocation")
    public String getWsdlLocation() {
        return wsdlLocation;
    }

    @JsonProperty("wsdlLocation")
    public void setWsdlLocation(String wsdlLocation) {
        this.wsdlLocation = wsdlLocation;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean isConstructISOMessage() {
        return constructISOMessage;
    }

    public void setConstructISOMessage(boolean constructISOMessage) {
        this.constructISOMessage = constructISOMessage;
    }

    public boolean isParseISOMessage() {
        return parseISOMessage;
    }

    public void setParseISOMessage(boolean parseISOMessage) {
        this.parseISOMessage = parseISOMessage;
    }

    @JsonProperty("status")
    public StatusType getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(StatusType status) {
        this.status = status;
    }

    @JsonProperty("enrichmentEnabled")
    public boolean isEnrichmentEnabled() {
        return enrichmentEnabled;
    }

    @JsonProperty("enrichmentEnabled")
    public void setEnrichmentEnabled(boolean enrichmentEnabled) {
        this.enrichmentEnabled = enrichmentEnabled;
    }
}