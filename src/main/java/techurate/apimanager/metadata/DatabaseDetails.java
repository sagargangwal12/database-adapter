package techurate.apimanager.metadata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "databaseadapter", uniqueConstraints =
@UniqueConstraint(columnNames = {"name"}))
@Data
public class DatabaseDetails {

    @JsonProperty("name")
    private String name;

    @Id
    @Column(name = "id")
    @JsonProperty("databaseAdapterID")
    private String databaseAdapterID;

    @JsonProperty("databaseUserName")
    private String databaseUserName;

    @JsonProperty("password")
    private String password;

    @JsonProperty("databaseUrl")
    private String databaseUrl;

    @JsonProperty("databaseSourceName")
    private String databaseSourceName;

    @JsonProperty("databaseDriverName")
    private String databaseDriverName;

    @JsonProperty("databaseType")
    private String databaseType;

    @JsonProperty("serviceID")
    private String serviceID;

    @JsonProperty("operationID")
    private String operationID;

}