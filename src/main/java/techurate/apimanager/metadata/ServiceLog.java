package techurate.apimanager.metadata;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ServiceLog {
    @Id
    @GeneratedValue
    @Column(name = "id")
    @JsonProperty("serviceLogId")
    private Long serviceLogId;

    @Column(name = "requestTimestamp1", columnDefinition = "timestamp with time zone not null")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestTimestamp1;
    private String method;
    private String url;
    private String ipAddress;
    private int requestSize;
    private String apiId;
    private String operationId;
    private String operationName;
    private String serviceId;
    private String serviceName;
    private String subscriptionId;
    private String userId;
    private String apiRegion;
    private double serviceTime;
    private double apiTime;
    private int responseSize;
    private String cache;
    private int backendResponseCode;
    private int responseCode;
    private String status;
    private String serviceType;
}
