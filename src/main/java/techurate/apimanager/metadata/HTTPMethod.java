package techurate.apimanager.metadata;

public enum HTTPMethod {
    GET("GET"), POST("POST"), DELETE("DELETE"), PUT("PUT"), OPTIONS("OPTIONS");

    private String method;

    private HTTPMethod(String method) {
        this.setMethod(method);
    }

    public String value() {
        return name();
    }

    public static HTTPMethod fromValue(String v) {
        return valueOf(v);
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}