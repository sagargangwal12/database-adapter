package techurate.apimanager.metadata;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Pagination {
    private String isPaginationRequired;
    private int fromPage;
    private int toPage;
}
