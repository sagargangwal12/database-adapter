package techurate.apimanager.metadata;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Entity
@Table(name = "classmetadata", indexes = @Index(
        name = "idx_servicedefinitionid",
        columnList = "jsonMapClass, source, type",
        unique = false
))
public class ClassMetadata {

    @Id
    @JsonProperty("metadataID")
    private String metadataID;

    @JsonProperty("fqnClassName")
    private String fqnClassName;

    @JsonProperty("serviceID")
    @Column(name = "servicedefinition_serviceid")
    private String serviceID;

    @JsonProperty("fieldList")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @OrderBy(value = "name ASC")
    @JoinColumn(name = "classmetadata_metadataid")
    private Set<FieldList> fieldList = null;

    @JsonProperty("type")
    @Enumerated(EnumType.STRING)
    private ClassType type;

    @JsonProperty("source")
    @Enumerated(EnumType.STRING)
    private Source source;

    @JsonProperty("jsonMapClass")
    private String jsonMapClass;

    @JsonProperty("system")
    private String system;

    @JsonIgnore
    @Transient
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("fqnClassName")
    public String getFqnClassName() {
        return fqnClassName;
    }

    @JsonProperty("fqnClassName")
    public void setFqnClassName(String fqnClassName) {
        this.fqnClassName = fqnClassName;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonProperty("metadataID")
    public String getMetadataID() {
        return metadataID;
    }

    @JsonProperty("metadataID")
    public void setMetadataID(String metadataID) {
        this.metadataID = metadataID;
    }

    @JsonProperty("type")
    public ClassType getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(ClassType type) {
        this.type = type;
    }

    @JsonProperty("source")
    public Source getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(Source source) {
        this.source = source;
    }

    @JsonProperty("jsonMapClass")
    public String getJsonMapClass() {
        return jsonMapClass;
    }

    @JsonProperty("jsonMapClass")
    public void setJsonMapClass(String jsonMapClass) {
        this.jsonMapClass = jsonMapClass;
    }

    public Set<FieldList> getFieldList() {
        return fieldList;
    }

    public void setFieldList(Set<FieldList> fieldList) {
        this.fieldList = fieldList;
    }

    public boolean equals(Object metadata) {
        if (metadata == null || !(metadata instanceof ClassMetadata)) {
            return false;
        }
        ClassMetadata cm1 = (ClassMetadata) metadata;
        if (this.getMetadataID().equalsIgnoreCase(cm1.getMetadataID())) {
            return true;
        }
        return false;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }
}