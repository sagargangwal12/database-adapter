package techurate.apimanager.metadata;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "fieldID",
        "mapField",
        "classMetadata",
        "path",
        "name",
        "description",
        "type",
        "defaultValue",
        "required",
        "length",
        "parent",
        "children",
        "prefix",
        "suffix"
})

@Entity
@Table(name = "field", indexes = @Index(
        name = "idx_parent_field",
        columnList = "parent_fieldid, classmetadata_metadataid",
        unique = false
))
public class FieldList {

    @Id
    @JsonProperty("id")
    private String id;

    @JsonProperty("mapField")
    private String mapField;

    @JsonProperty("path")
    private String path;

    @JsonProperty("name")
    private String name;

    @JsonProperty("description")
    private String description;

    @JsonProperty("type")
    private String type;

    @JsonProperty("defaultValue")
    private String defaultValue;

    @JsonProperty("required")
    private Boolean required;

    @JsonProperty("length")
    private Integer length;

    @JsonProperty("mask")
    @Column(name = "maskEnabled")
    private Boolean bMasked;

    @JsonProperty("prefix")
    @Column(name = "prefix")
    private String prefix;

    @JsonProperty("suffix")
    @Column(name = "suffix")
    private String suffix;

    @JsonIgnore
    @Transient
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_fieldid")
    @OrderBy(value = "name ASC")
    @JsonProperty("children")
    @JsonManagedReference
    private Set<FieldList> children;

    @ManyToOne
    @JoinColumn(name = "parent_fieldid")
    @JsonProperty("parent")
    @JsonBackReference
    private FieldList parent;

    @JsonProperty("validationRegEx")
    private String validationRegEx;


    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("defaultValue")
    public String getDefaultValue() {
        return defaultValue;
    }

    @JsonProperty("defaultValue")
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @JsonProperty("required")
    public Boolean getRequired() {
        return required;
    }

    @JsonProperty("required")
    public void setRequired(Boolean required) {
        this.required = required;
    }

    @JsonProperty("length")
    public Integer getLength() {
        return length;
    }

    @JsonProperty("length")
    public void setLength(Integer length) {
        this.length = length;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonProperty("id")
    public String getID() {
        return id;
    }

    @JsonProperty("id")
    public void setID(String id) {
        this.id = id;
    }

    @JsonProperty("parent")
    public FieldList getParent() {
        return parent;
    }

    @JsonProperty("parent")
    public void setParent(FieldList parent) {
        this.parent = parent;
    }

    @JsonProperty("children")
    public Set<FieldList> getChildren() {
        return children;
    }

    @JsonProperty("children")
    public void setChildren(Set<FieldList> children) {
        this.children = children;
    }

    @JsonProperty("mapField")
    public String getMapField() {
        return mapField;
    }

    @JsonProperty("mapField")
    public void setMapField(String mapField) {
        this.mapField = mapField;
    }

    @JsonProperty("path")
    public String getPath() {
        return path;
    }

    @JsonProperty("path")
    public void setPath(String path) {
        this.path = path;
    }

    public String toString() {
        return this.getName();
    }

    @JsonProperty("mask")
    public Boolean isMasked() {
        if (bMasked == null) return false;
        return bMasked;
    }

    @JsonProperty("mask")
    public void setMasked(Boolean bMasked) {
        this.bMasked = bMasked;
    }

    @JsonProperty("validationRegEx")
    public String getValidationRegEx() {
        return validationRegEx;
    }

    @JsonProperty("validationRegEx")
    public void setValidationRegEx(String validationRegEx) {
        this.validationRegEx = validationRegEx;
    }

    @JsonProperty("prefix")
    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @JsonProperty("suffix")
    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

}