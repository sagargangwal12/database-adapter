package techurate.apimanager.metadata;

public enum ClassType {
    Request("Request"), Response("Response");

    private String type;

    private ClassType(String type) {
        this.setType(type);
    }

    public String value() {
        return name();
    }

    public static ClassType fromValue(String v) {
        return valueOf(v);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
