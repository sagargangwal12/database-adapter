package techurate.apimanager.metadata;

public enum ContentType {
    XML("application/xml", "XSD", "application/xml; utf-8"), JSON("application/json", "JSON", "application/json; utf-8");

    private String contentType;
    private String format;
    private String httpFormat;

    private ContentType(String contentType, String format, String httpFormat) {
        this.setContentType(contentType);
        this.setFormat(format);
        this.setHttpFormat(httpFormat);
    }

    public String value() {
        return name();
    }

    public static ContentType fromValue(String v) {
        return valueOf(v);
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getHttpFormat() {
        return httpFormat;
    }

    public void setHttpFormat(String httpFormat) {
        this.httpFormat = httpFormat;
    }

}
