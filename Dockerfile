FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD target/docker-database-adapter.jar docker-database-adapter.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","docker-database-adapter.jar"]